# Modified Swim Calculator
A simple calculator app based on modifications made to the Swim "to-do list" example project

# Original To-do List Example Repo
https://github.com/swimos/to-do-list

## Setup, Build, and Run:

You need java9+ to run the application and these instructions only cover running in a unix like environment. Gradle is not required as the project provides a gradle wrapper to use instead.

1. `git clone https://gitlab.com/cristafalk/modified-swim-todo-calculator.git`

2. `cd modified-swim-todo-calculator/server`

3. `./gradlew run`

4. Navigate to `http://127.0.0.1:9001` and you should see a blank calculator

5. The top two editable inputs are for your two operand values (number #1 and number #2). Operations are performed by clicking the math buttons below after you specify your two operands. Doing so will generate a result that appears below on the calculator, along with a delete button for each calculation in your history.

6. Adding `?list=<listid>` to the url will create a calculator instance for that listID. for example `http://127.0.0.1:9001?list=tipCalcultor` will create a new ListAgent with the ID of 'tipCalculator'.
